import unicodecsv as csv
from yelp.client import Client
from yelp.oauth1_authenticator import Oauth1Authenticator
import json

# read API keys
with open('../../config_secret.json') as cred:
    creds = json.load(cred)
    auth = Oauth1Authenticator(**creds)
    client = Client(auth)

search_filters = ['restaurants','grocery','active','localservices,shopping', 'nightlife','publicservicesgovt', 'financialservices,professional','health','religiousorgs'] 
# search_filters = ['restaurants','localservices,shopping']

# restaurants = restaurants
# grocery stores = grocery
# exercise & recreation = active
# shopping & retail = localservices + shopping
# bars & nightlife = nightlife
# civic & public = publicsericesgovt
# professional services = financialservices + professional
# hospital & medical = health
# religious organizations

# with open('kendall2.csv','w') as seaport:
# 	seaport.write('Name'+ ',' + 'Reviews' + ',' + 'Category' + '\n')
# 	seaport.close()

with open('data/data-zoomed.csv','w') as file:
	file.write('id' + ',' + 'y' + ',' + 'x' + ',' + 'name'+ ',' + 'category' + ',' + 'reviews' + ',' + 'rating' + ',' + 'address' + ',' + 'lat' + ',' + 'lon' + ',' + 'neighborhood' + '\n')
	writer = csv.writer(file, dialect='excel')
	for term in search_filters:
		print term
		off = 0
		while (off < 500):
			bounds = [30.218958, -97.837085, 30.404793, -97.610492]
			search_results = client.search_by_bounding_box(42.343460, -83.047923, 42.355449, -83.033160, category_filter=term,offset=off, limit=20)
			print search_results
			for x in search_results.businesses:
				try:
					name = x.name
					id = x.id
					count = str(x.review_count)
					rating = x.rating
					address = x.location.address[0]
					lat = x.location.coordinate.latitude
					lon = x.location.coordinate.longitude
					if term == 'localservices,shopping':
						term = 'shopping'
					# else if term == 				
				except:
					lat = 0
					lon = 0
				writer.writerow([x.id,lon,lat,x.name,term,count,rating, address,lat,lon,"Paris"])
				print x.name +"," + count + "," + term + x.id
			print off
			off = off + 20
file.close()



